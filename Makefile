CFLAGS ?= -Wall

CFLAGS += -Dpicolangdir=\"/usr/share/pico/lang\"
LDLIBS = -lpopt -lttspico

all: pico2wave pico2wave.1

%.1: %.md
	marked-man --version 1.1 --section 1 --manual 'pico2wave man page' $< > $@
