# pico2wave - command line text-to-speech converter

## SYNOPSIS
```
pico2wave -w /tmp/test.wav 'Text to speech.'
```

## DESCRIPTION
`pico2wave` is a command-line utilty to convert text to speech using the SVOX Pico engine.

Options:

-w, --wave=filename.wav
      Write output to this WAV file (extension SHOULD be .wav)

-l, --lang=lang
      Language (default: "en-US"; supported languages: "en-US", "en-GB", "de-DE", "es-ES", "fr-FR" and "it-IT")

Help options:

-?, --help
      Show an help message

--usage
      Display brief usage message

## EXAMPLES
Either supply the text on the command line:
```
pico2wave -w /tmp/test.wav -l en-US 'Just a test for text to speech.' && aplay /tmp/test.wav
pico2wave -w /tmp/test.wav -l it-IT "Solo un test di conversione." && aplay /tmp/test.wav
```
or pipe it through `stdin` (in this case, due to SVOX limitations the text will be truncated to 32766 characters maximum):
```
cat test.txt | pico2wave -w /tmp/test.wav -l it-IT && aplay /tmp/test.wav
```

## AUTHORS
Copyright (C) 2009-2013 Mathieu Parent, 2017 Paolo Greppi

## COPYRIGHT
This manual page was written by Paolo Greppi `<paolo.greppi@libpf.com>` for the Debian project (and may be used by others).
